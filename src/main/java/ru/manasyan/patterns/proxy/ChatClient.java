package ru.manasyan.patterns.proxy;

import ru.manasyan.patterns.proxy.IChatClient;
import ru.manasyan.patterns.proxy.model.Message;

public class ChatClient implements IChatClient {

    private Message message;

    @Override
    public void sendMessage(Message message) {
        System.out.println("Sending " + message);
        this.message = message;
    }

    @Override
    public Message getMessage() {
        System.out.println("Getting " + message);
        return message;
    }
}
