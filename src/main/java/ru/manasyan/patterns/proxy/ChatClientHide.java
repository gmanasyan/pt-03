package ru.manasyan.patterns.proxy;

import ru.manasyan.patterns.proxy.model.Message;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.UUID;

import static java.util.Objects.isNull;

public class ChatClientHide implements InvocationHandler {

    private HashMap<String, String> usersList = new HashMap<>();

    private IChatClient chatClient;

    public ChatClientHide(IChatClient chatClient) {
        this.chatClient = chatClient;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        System.out.print("> Proxy Hide");
        if ("sendMessage".equals(method.getName())) {
            System.out.print("> sendMessage");
            Message message = (Message) args[0];
            message.setAuthor(hide(message.getAuthor()));
            message.setDestination(hide(message.getDestination()));
            Object o = method.invoke(chatClient, args);
            return o;
        }
        if ("getMessage".equals(method.getName())) {
            System.out.print("> getMessage");
            Message message = (Message) method.invoke(chatClient, args);
            message.setAuthor(deHide(message.getAuthor()));
            message.setDestination(deHide(message.getDestination()));
            return message;
        }
        Object o = method.invoke(chatClient, args);
        return o;
    }

    private String crypto(String message, String password) {
        StringBuilder result = new StringBuilder();
        int pasIndex = 1;
        for (int i = 0; i < message.length() ; i++) {
            result.append(Character.toChars(message.charAt(i) ^ password.charAt(pasIndex)));
            pasIndex += 1;
            if (pasIndex >= password.length()) pasIndex = 1;
        }
        return result.toString();
    }

    private String hide(String user) {
        String userCode = usersList.get(user);
        if (isNull(userCode)) {
            UUID code = UUID.randomUUID();
            usersList.put(user, code.toString());
            return code.toString();
        }
        return userCode;
    }

    private String deHide(String code) {
        String userName;
        try {
            userName = usersList.entrySet().stream()
                    .filter(x -> x.getValue().equals(code))
                    .findFirst().get().getKey();
        } catch (Exception ex) {
            userName = "<<USER NOT FOUND>>";
        }
        return userName;
    }

}
