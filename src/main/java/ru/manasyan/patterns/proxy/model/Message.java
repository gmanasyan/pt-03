package ru.manasyan.patterns.proxy.model;

public class Message {

    private String author;

    private String destination;

    private String bodyMessage;

    public Message(String author, String destination, String bodyMessage) {
        this.author = author;
        this.destination = destination;
        this.bodyMessage = bodyMessage;
    }

    public String getAuthor() {
        return author;
    }

    public String getDestination() {
        return destination;
    }

    public String getBodyMessage() {
        return bodyMessage;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public void setBodyMessage(String bodyMessage) {
        this.bodyMessage = bodyMessage;
    }

    @Override
    public String toString() {
        return "Message{" +
                "author='" + author + '\'' +
                ", destination='" + destination + '\'' +
                ", bodyMessage='" + bodyMessage + '\'' +
                '}';
    }
}
