package ru.manasyan.patterns.proxy;

import ru.manasyan.patterns.proxy.model.Message;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class ChatClientCrypto implements InvocationHandler {

    private static final String PASSWORD = "1234";

    private IChatClient chatClient;

    public ChatClientCrypto(IChatClient chatClient) {
        this.chatClient = chatClient;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        System.out.print("> Proxy Crypto");
        if ("sendMessage".equals(method.getName())) {
            System.out.print("> sendMessage");
            Message message = (Message) args[0];
            message.setBodyMessage(crypto(message.getBodyMessage(), PASSWORD));
            Object o = method.invoke(chatClient, args);
            return o;
        }
        if ("getMessage".equals(method.getName())) {
            System.out.print("> getMessage");
            Message o = (Message) method.invoke(chatClient, args);
            o.setBodyMessage(crypto(o.getBodyMessage(), PASSWORD));
            return o;
        }
        Object o = method.invoke(chatClient, args);
        return o;
    }

    private String crypto(String message, String password) {
        StringBuilder result = new StringBuilder();
        int pasIndex = 1;
        for (int i = 0; i < message.length() ; i++) {
            result.append(Character.toChars(message.charAt(i) ^ password.charAt(pasIndex)));
            pasIndex += 1;
            if (pasIndex >= password.length()) pasIndex = 1;
        }
        return result.toString();
    }

}
