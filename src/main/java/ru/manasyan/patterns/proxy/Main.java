package ru.manasyan.patterns.proxy;

import ru.manasyan.patterns.proxy.IChatClient;
import ru.manasyan.patterns.proxy.model.Message;

import java.lang.reflect.Proxy;

public class Main {

    public static void main(String[] args) {

        IChatClient chatClient = new ChatClient();
        chatClient.sendMessage(new Message("Georgy Manasyan",
                "Ilya Sukhachev",
                "Hello there. Here is simple XOR crypto algorithm"));
        System.out.println(chatClient.getMessage());

        System.out.println(">>> Security Proxy");
        ClassLoader classLoader = ChatClientCrypto.class.getClassLoader();
        Class<?>[] interfaces = new Class[]{IChatClient.class};
        ChatClientCrypto invocationHandler = new ChatClientCrypto(chatClient);

        IChatClient chatCrypto = (IChatClient) Proxy.newProxyInstance(
                classLoader,
                interfaces,
                invocationHandler
        );

        chatCrypto.sendMessage(new Message("Georgy Manasyan",
                "Ilya Sukhachev",
                "Hello there. Here is simple XOR crypto algorithm"));
        System.out.println(chatCrypto.getMessage());

        System.out.println(">>> Hide Proxy");
        ClassLoader classLoader2 = ChatClientHide.class.getClassLoader();
        Class<?>[] interfaces2 = new Class[]{IChatClient.class};
        ChatClientHide invocationHandler2 = new ChatClientHide(chatCrypto);

        IChatClient chatHide = (IChatClient) Proxy.newProxyInstance(
                classLoader2,
                interfaces2,
                invocationHandler2
        );

        chatHide.sendMessage(new Message("Georgy Manasyan",
                "Ilya Sukhachev",
                "Hello there. Here is simple XOR crypto algorithm"));
        System.out.println(chatHide.getMessage());
    }

}
