package ru.manasyan.patterns.proxy;

import ru.manasyan.patterns.proxy.model.Message;

public interface IChatClient {

    void sendMessage(Message message);
    Message getMessage();

}
