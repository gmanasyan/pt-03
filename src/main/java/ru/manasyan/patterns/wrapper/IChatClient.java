package ru.manasyan.patterns.wrapper;

import ru.manasyan.patterns.wrapper.model.Message;

public interface IChatClient {

    void sendMessage(Message message);
    Message getMessage();

}
