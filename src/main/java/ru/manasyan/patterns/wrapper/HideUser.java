package ru.manasyan.patterns.wrapper;

import ru.manasyan.patterns.wrapper.model.Message;

import java.util.HashMap;
import java.util.UUID;

import static java.util.Objects.isNull;

public class HideUser extends ChatClientDecorator {

    private HashMap<String, String> usersList = new HashMap<>();

    public HideUser(IChatClient chatClient) {
        super(chatClient);
    }

    @Override
    public void sendMessage(Message message) {
        System.out.println("> Hiding users: " + message);
        message.setAuthor(hide(message.getAuthor()));
        message.setDestination(hide(message.getDestination()));
        super.sendMessage(message);
    }

    @Override
    public Message getMessage() {
        System.out.println("> De-hiding users: ");
        Message message = super.getMessage();
        message.setAuthor(deHide(message.getAuthor()));
        message.setDestination(deHide(message.getDestination()));
        return message;
    }

    private String hide(String user) {
        String userCode = usersList.get(user);
        if (isNull(userCode)) {
            UUID code = UUID.randomUUID();
            usersList.put(user, code.toString());
            return code.toString();
        }
        return userCode;
    }

    private String deHide(String code) {
        String userName;
        try {
            userName = usersList.entrySet().stream()
                    .filter(x -> x.getValue().equals(code))
                    .findFirst().get().getKey();
        } catch (Exception ex) {
            userName = "<<USER NOT FOUND>>";
        }
        return userName;
    }


}
