package ru.manasyan.patterns.wrapper;

import ru.manasyan.patterns.wrapper.model.Message;

public class ChatClientDecorator implements IChatClient {

    private IChatClient chatClient;

    public ChatClientDecorator(IChatClient chatClient) {
        this.chatClient = chatClient;
    }

    @Override
    public void sendMessage(Message message) {
        this.chatClient.sendMessage(message);
    }

    @Override
    public Message getMessage() {
        return this.chatClient.getMessage();
    }
}
