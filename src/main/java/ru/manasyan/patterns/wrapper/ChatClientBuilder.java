package ru.manasyan.patterns.wrapper;

public class ChatClientBuilder {

    private IChatClient chatClient;

    public ChatClientBuilder(IChatClient chatClient) {
        this.chatClient = chatClient;
    }

    public ChatClientBuilder withCrypto(String password) {
        chatClient = new Crypto(chatClient, password);
        return this;
    }

    public ChatClientBuilder withUserHide() {
        chatClient = new HideUser(chatClient);
        return this;
    }

    public IChatClient build() {
        return chatClient;
    }

}
