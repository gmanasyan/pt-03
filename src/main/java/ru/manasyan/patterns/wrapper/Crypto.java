package ru.manasyan.patterns.wrapper;

import ru.manasyan.patterns.wrapper.model.Message;

public class Crypto extends ChatClientDecorator {

    private final String password;

    public Crypto(IChatClient chatClient, String password) {
        super(chatClient);
        this.password = password;
    }

    @Override
    public void sendMessage(Message message) {
        System.out.println("> Crypto: " + message);
        System.out.println("> Crypto password: " + password);
        message.setBodyMessage(crypto(message.getBodyMessage(), password));
        super.sendMessage(message);
    }

    @Override
    public Message getMessage() {
        System.out.println("> Crypto password: " + password);
        Message message = super.getMessage();
        message.setBodyMessage(crypto(message.getBodyMessage(), password));
        return message;
    }

    private String crypto(String message, String password) {
        StringBuilder result = new StringBuilder();
        int pasIndex = 1;
        for (int i = 0; i < message.length() ; i++) {
            result.append(Character.toChars(message.charAt(i) ^ password.charAt(pasIndex)));
            pasIndex += 1;
            if (pasIndex >= password.length()) pasIndex = 1;
        }
        return result.toString();
    }

}
