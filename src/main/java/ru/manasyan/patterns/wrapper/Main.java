package ru.manasyan.patterns.wrapper;

import ru.manasyan.patterns.wrapper.model.Message;

public class Main {

    public static void main(String[] args) {

        IChatClient chatClient = new ChatClient();
        chatClient.sendMessage(new Message("Georgy Manasyan",
                "Ilya Sukhachev",
                "Hello there. Here is simple XOR crypto algorithm"));
        System.out.println(chatClient.getMessage());


        System.out.println(">>> Wrappers");
        chatClient = new ChatClientBuilder(chatClient)
                .withCrypto("12345")
                .withUserHide()
                .build();
        chatClient.sendMessage(new Message("Georgy Manasyan",
                "Ilya Sukhachev",
                "Hello there. Here is simple XOR crypto algorithm"));
        System.out.println(chatClient.getMessage());

    }

}
